package com.hse.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebCrawler {

    private int i;
    private HashSet<String> links;
    private Map<String, String> pageMap = new HashMap<String, String>();
    private Pattern pattern;
    private int depth;

    public WebCrawler(int depth) {
        pattern = Pattern.compile("([a-z][a-z0-9+\\-.]*:([^?#]+)?)?");
        links = new HashSet<String>();
        this.depth= depth;
    }

    public Map<String, String> getPageLinks(String URL) {
        if (!links.contains(URL) && i < depth) {
            i++;
            try {
                if (links.add(URL)) {
                    System.out.println(URL);
                }

                Document document = Jsoup.connect(URL).get();
                String html = document.toString();
                pageMap.put(URL, html);

                Elements linksOnPage = document.select("a[href]");

                for (Element page : linksOnPage) {

                    Matcher matcher = pattern.matcher(page.attr("abs:href"));
                    if (matcher.find()) {
                        String path = matcher.group(2);
                        getPageLinks("https:" + path);
                    }
                }
            } catch (IOException e) {
                System.err.println("For '" + URL + "': " + e.getMessage());
            }
        }
        return pageMap;
    }

}
