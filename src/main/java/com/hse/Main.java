package com.hse;

import com.hse.bm25.BM25;
import com.hse.bm25.ContentUtil;
import com.hse.crawler.WebCrawler;

import java.util.Map;

import static com.hse.bm25.ContentUtil.countAvgDocLength;
import static com.hse.bm25.ContentUtil.countWordInDocuments;

public class Main {

    public static void main(String[] args) {
        String targetWord = "spring";

        int depth = 20;
        Map<String, String> pageMap = new WebCrawler(depth).getPageLinks("http://www.mkyong.com/tutorials/java-8-tutorials/");

        int docNumber = pageMap.values().size();
        int pageNumberWithWord = countWordInDocuments(targetWord, pageMap.values());
        int avgDocLength = countAvgDocLength(pageMap.values());

        BM25 bm25 = new BM25();


        for (Map.Entry<String, String> entryPage : pageMap.entrySet()) {
            String url = entryPage.getKey();
            String page = entryPage.getValue();

            String[] words = page.split("\\s+");

            int qtf = ContentUtil.countWord(words, targetWord);
            int docLength = words.length;


            float score = bm25.computeScore(1, qtf, docLength, avgDocLength, docNumber, pageNumberWithWord);

            System.out.println("Page score for url " + url + " is : " + score);
        }
    }
}
