package com.hse.bm25;

public class BM25 {
    private float k1 = 1.2f;
    private float k2 = 100f;
    private float b = 0.75f;

    public float computeScore(int qTf, int docTf, int docLength, int avgDocLength, int docCount, int df) {
        float K = k1 * ((1 - b) + b * (docLength / avgDocLength));
        float idf = (float) Math.log((docCount - df + 0.5f) / (df + 0.5f));
        float val = ((k1 + 1.0f) * docTf / (K + docTf)) * ((k2 + 1.0f) * qTf) / (k2 + qTf);

        return idf * val;
    }
}
